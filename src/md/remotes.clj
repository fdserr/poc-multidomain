(ns md.remotes
  (:require
   [datomic.api :as d]))

; should fetch from env
(def urls
  #:remote{:onze "datomic:mem://db-onze"
           :tnt "datomic:mem://db-tnt"})
