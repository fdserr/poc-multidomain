(ns md.tnt
  (:require
   [md.remotes :as remotes]
   [datomic.api :as d]))

(declare dbfn-change-pw)

(defn conn-tnt []
  (let [db-url (:remote/tnt remotes/urls)
        created (d/create-database db-url)
        conn (d/connect db-url)]
    @(d/transact conn [#:db{:ident :user/email
                            :valueType :db.type/string
                            :cardinality :db.cardinality/one
                            :unique :db.unique/value}
                       #:db{:ident :user/pw
                            :valueType :db.type/string
                            :cardinality :db.cardinality/one}
                       #:db{:ident :change-pw
                            :fn (d/function
                                 {:lang "clojure"
                                  :params '[db email old-pw new-pw]
                                  :code dbfn-change-pw})}])
    (when created
      @(d/transact conn [#:user{:email "joe@strummer.com"
                                :pw "joe"}
                         #:user{:email "mick@jones.com"
                                :pw "mick"}]))
    conn))

(def dbfn-change-pw
  '(let [user (d/entity db [:user/email email])
         _ (when-not user
             (throw (ex-info (format "User not found: %s" email) {})))
         db-pw (:user/pw user)
         _ (when-not (= db-pw old-pw)
             (throw (ex-info "Old password doesn't match" {})))]
     [{:db/id [:user/email email] :user/pw new-pw}]))

(defn change-pw
  [email old-pw new-pw]
  (let [db-after
        (:db-after
         @(d/transact (conn-tnt)
           [[:change-pw email old-pw new-pw]]))]
    (into {} (d/touch (d/entity db-after [:user/email email])))))

(defn challenge-pw?
  [email pw]
  (let [user (d/entity (d/db (conn-tnt)) [:user/email email])
        db-pw (:user/pw user)]
    (= pw db-pw)))
