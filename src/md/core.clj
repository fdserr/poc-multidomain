(ns md.core
  (:require
   [clojure.core.async :refer [thread]]
   [datomic.api :as d]
   [md.onze :as onze]
   [md.remotes :as remotes]
   [md.tnt :as tnt]))

(def change-pw tnt/change-pw)
(def challenge-pw? tnt/challenge-pw?)

(def create-je onze/create-je)
(def audit-user onze/audit-user)


;;;

(defn start-bcast!
  [*state bcast-fn]
  (reset!
   *state
   (reduce-kv
    (fn [m k v]
      (let [c (d/connect v)
            q (d/tx-report-queue c)]
        (thread (loop [] (bcast-fn [k (.take q)]) (recur)))
        (assoc m k [c q])))
    {}
    remotes/urls)))
