(ns md.main
  (:require
   [datomic.api :as d]
   [org.httpkit.server :as server]))

(defn app [req]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    (str (:conn req))})

(defn wrap-conn
  [handler conn]
  (fn [req]
    (handler (assoc req :conn conn))))

(defn -main []
  (let [port (Integer/parseInt
              (or (System/getenv "SERVER_PORT")
                  "8080"))
        db-url (or (System/getenv "DATABASE_URL")
                   (str "datomic:mem://" (d/squuid)))
        conn (do
               (d/create-database db-url)
               (d/connect db-url))]
    (printf "Server listening on port %s. \n DB: %s" port db-url)
    (server/run-server (wrap-conn app conn) {:port port})))
