(ns md.onze
  (:require
   [md.remotes :as remotes]
   [datomic.api :as d]))

(declare dbfn-create-je)

(defn conn-onze []
  (let [db-url (:remote/onze remotes/urls)
        created (d/create-database db-url)
        conn (d/connect db-url)]
    @(d/transact conn [#:db{:ident :je/narrative
                            :valueType :db.type/string
                            :cardinality :db.cardinality/one
                            :unique :db.unique/value}
                       #:db{:ident :user/remote
                            :valueType :db.type/tuple
                            :tupleTypes [:db.type/keyword :db.type/long]
                            :cardinality :db.cardinality/one
                            :unique :db.unique/identity}
                       #:db{:ident :tx/user
                            :valueType :db.type/ref
                            :cardinality :db.cardinality/one}
                       #:db{:ident :create-je
                            :fn (d/function
                                 {:lang "clojure"
                                  :requires '[[md.remotes :as remotes]]
                                  :params '[db email narrative]
                                  :code dbfn-create-je})}])
    conn))

(def dbfn-create-je
  '(let [remote-user (d/entity
                      (d/db (d/connect (:remote/tnt remotes/urls)))
                      [:user/email email])
         _ (when-not remote-user
             (throw (ex-info (format "Remote: user not found: %s" email) {})))]
     [{:db/id "user"
       :user/remote [:remote/tnt (:db/id remote-user)]}
      {:je/narrative narrative}
      {:db/id "datomic.tx"
       :tx/user "user"}]))

(defn create-je
  [email narrative]
  (let [db-after
        (:db-after
         @(d/transact (conn-onze)
           [[:create-je email narrative]]))]
    (into {} (d/touch (d/entity db-after [:je/narrative narrative])))))

(defn audit-user
  [email]
  (d/q '[:find [?narrative ...]
         :in $onze $tnt ?email
         :where
         [$tnt ?remote-user :user/email ?email]
         [$onze (ground :remote/tnt) ?remote]
         [$onze (tuple ?remote ?remote-user) ?tuple]
         [$onze ?user :user/remote ?tuple]
         [$onze ?tx :tx/user ?user]
         [$onze _ :je/narrative ?narrative ?tx]]
       (d/db (conn-onze))
       (d/db (d/connect (:remote/tnt remotes/urls)))
       email))
