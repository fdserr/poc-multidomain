(require
 '[md.core :as md :reload true]
 '[datomic.api :as d])
 ; '[org.httpkit.server :as server])


;;; tnt

(md/change-pw "joe@strummer.com" "joe" "jim")
(md/change-pw "joe@strummer.com" "jim" "joe")

(md/challenge-pw? "joe@strummer.com" "joe")

;;; onze

(md/create-je "joe@strummer.com" "Bought expensive stuff")
(md/create-je "joe@strummer.com" "Bought inexpensive stuff")
(md/create-je "joe@strummer.com" "Bought cheap stuff")

(md/audit-user "joe@strummer.com")
(md/audit-user "paul@simonon.com")

;;; bcast

(def *state (atom {}))
(md/start-bcast! *state #(println "[BCAST]" %))
